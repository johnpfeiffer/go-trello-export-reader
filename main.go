package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"time"

	"bitbucket.org/johnpfeiffer/go-jsondao"
)

// Board is a Trello Board
type Board struct {
	ID               string    `json:"id"`
	Name             string    `json:"name"`
	Pinned           bool      `json:"pinned"`
	Closed           bool      `json:"closed"`
	DateLastActivity time.Time `json:"dateLastActivity"` //	t, _ := time.Parse(time.RFC3339, "2016-08-12T17:26:47.652Z")
	DateLastView     time.Time `json:"dateLastView"`
	ShortURL         string    `json:"shortUrl"` // should be url.URL
	ShortLink        string    `json:"shortLink"`
	Starred          bool      `json:"starred"`
	Lists            []List    `json:"lists"`
	Cards            []Card    `json:"cards"`

	// desc
	// descData

	// url
	// idTags
	// idOrganization
	// actions
	// labels
	// prefs
	// pluginData
	// powerUps
	// invitations
	// labelNames
	// checklists
	// members	// the list of users
	// memberships
	// subscribed
	// invited
	// datePluginDisable
}

// List is a list in a trello board
type List struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	Closed bool   `json:"closed"`
	Cards  []Card
	//		"idBoard": "553",
	//		"pos": 4095.9375,
	//		"subscribed": false
}

// Card is a card in a trello board
type Card struct {
	ID               string    `json:"id"`
	Name             string    `json:"name"`
	Closed           bool      `json:"closed"`
	DateLastActivity time.Time `json:"dateLastActivity"`
	BoardID          string    `json:"idBoard"`
	ListID           string    `json:"idList"`

	/*
		"attachments": [],
		"badges": {
			"attachments": 0,
			"checkItems": 0,
			"checkItemsChecked": 0,
			"comments": 0,
			"description": false,
			"due": null,
			"dueComplete": false,
			"fogbugz": "",
			"subscribed": false,
			"viewingMemberVoted": false,
			"votes": 0
		},
		"checkItemStates": null,
		"desc": "",
		"descData": null,
		"due": null,
		"dueComplete": false,
		"email": "johnsmith+2k0umcv1mhbc9ecup+2e33dq8ps59fqp+1h459lv6@boards.trello.com",
		"idAttachmentCover": null,

		"idChecklists": [],
		"idLabels": [],

		"idMembers": [],
		"idMembersVoted": [],
		"idShort": 31,
		"labels": [],
		"manualCoverAttachment": false,
		"name": "CLI-Help.txt",
		"pluginData": [],
		"pos": 32767.75,
		"shortLink": "392GYGZS",
		"shortUrl": "https://trello.com/c/32GYZS",
		"subscribed": false,
		"url": "https://trello.com/c/32GYZS/31-cli-helptxt"
	*/
}

func main() {

	f := "2017-06-trello-ci-board-export.json"
	parseBoardRaw(f)

	raw, err := ioutil.ReadFile(f)
	exitIfError(err)
	board := parseBoard(raw)
	fmt.Println(len(board.Cards), "cards")

	fmt.Printf("\nLists in %s (last active %s): \n", board.Name, board.DateLastActivity)
	for _, list := range board.Lists {
		fmt.Println("  ", list.Name, " , closed:", list.Closed, " ID:", list.ID)
		fmt.Println(len(list.Cards))
		for _, card := range list.Cards {
			fmt.Println(card)
		}
	}

}

func parseBoard(raw []byte) Board {
	var board Board
	err := json.Unmarshal(raw, &board)
	exitIfError(err)
	for _, card := range board.Cards {
		// TODO: a map or something besides brute force nesting
		for i, list := range board.Lists {
			if card.ListID == list.ID {
				board.Lists[i].Cards = append(list.Cards, card)
			}

		}
	}
	return board
}

func parseBoardRaw(path string) {
	m, err := jsondao.FileToMap(path)
	exitIfError(err)
	fmt.Println(len(m), "json fields found")
	for k := range m {
		fmt.Println("  ", k)
	}
}

func exitIfError(err error) {
	if err != nil {
		panic(err)
	}
}
